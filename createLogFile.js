const fs = require("fs");

function createLogFile(path, name, data) {
  let ts = Date.now();
  let date_ob = new Date(ts);
  let date = date_ob.getDate();
  let month = date_ob.getMonth() + 1;
  let year = date_ob.getFullYear();

  let monthStr = "";
  if (month <= 9) {
    monthStr = "0" + month.toString();
  } else {
    monthStr = month.toString();
  }

  let dateStr = "";
  if (date <= 9) {
    dateStr = "0" + date.toString();
  } else {
    dateStr = date.toString();
  }

  const logFile =
    "logs-" + name + "-" + year + "-" + monthStr + "-" + dateStr + ".txt";

  try {
    fs.appendFile(
      __dirname + "/logs/" + path + "/" + name + "/" + logFile,
      JSON.stringify(data) + "\n",
      function (err) {
        if (err) throw err;
        //file written successfully
      }
    );
  } catch (err) {
    console.error(err);
  }
}

module.exports = {
  createLogFile,
};
