// service test-service start
const express = require("express");
const https = require("https");
// const mariadb = require("mariadb/callback");
const mysql = require("mysql");
const jwt = require("jsonwebtoken");
var ip = require("ip");

const logs = require("./createLogFile");
// console.dir(ip.address());

// const conn = mysql.createConnection({
//   host: "172.29.70.129",
//   database: "trackingdrugs",
//   user: "famz",
//   password: "quiu7eedem",
// });

// conn.connect((err) => {
//   if (err) throw err;
//   console.log("Connected!");
// });

const app = express();
app.use(express.json());

app.get("/api/testservice", (req, res) => {
  res.send("Call this service success!");
})

app.get("/api/:service", (req, res) => {
  switch (req.params.service) {
    case "TestGet":
      // let result = getAll();
      res.send("Test get service");
      break;

    default:
      break;
  }
});

// 1.Test service clinicQueue
//    - android application
app.post("/api/doctor/:service", (req, res) => {
  let service = req.params.service;
  let result = [];
  let data;
  let body = req.body;
  let date_d;
  switch (service) {
    case "clinic":
      let perid = body.perid;
      date_d = body.date_d;
      data = JSON.stringify({
        service: "GETDOCTORDATA",
        perid: perid,
        date_d: date_d,
      });
      console.log(perid);
      console.log(date_d);
      break;
    case "clinicqueue":
      date_d = body.date_d;
      let code_b = body.code_b;
      let c_doct = body.c_doct;
      data = JSON.stringify({
        service: "GETDOCTORPATIENTQUEUE",
        date_d: date_d,
        code_b: code_b,
        c_doct: c_doct,
      });
      break;
    default:
      break;
  }

  var options = {
    host: "his01.psu.ac.th",
    path: "/HosApp/clinicq/update2his.php",
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Content-Length": Buffer.byteLength(data),
    },
    rejectUnauthorized: false,
  };

  var request = https.request(options, (httpres) => {
    var resdata = "";
    httpres.on("data", function (chunk) {
      resdata += chunk;
    });
    httpres.on("end", function () {
      let dataJSON = JSON.parse(resdata);
      if (dataJSON.length != 0) {
        let dataresponse = [];
        switch (service) {
          case "clinic":
            dataJSON.forEach((element) => {
              dataresponse.push({
                c_doct: element[0],
                date_d: element[1],
                clinic_code: element[2],
                clinic_name: element[3],
              });
            });

            break;
          case "clinicqueue":
            dataJSON.forEach((element) => {
              dataresponse.push({
                hn: element[0],
                date_d: element[1],
                regist_time: element[3],
                q_time: element[4],
                doct_time: element[5],
                close_time: element[6],
                nurse_time: element[7],
              });
            });
            break;
          default:
            break;
        }

        result = {
          statusCode: 200,
          date: new Date(),
          service: req.params.service,
          data: dataresponse,
        };
      } else {
        result = {
          statusCode: 400,
          date: new Date(),
          service: req.params.service,
          data: [],
        };
      }
      res.send(result);
    });
  });
  request.on("error", function (e) {
    let error = {
      statusCode: 400,
      error: e.message,
    };
    console.log(error);
  });
  request.write(data);
  request.end();
});

app.get("/api/sendsms", (req, res) => {
  let body = req.body;
  let perid = body.perid;
  console.log(perid);
  // let mobilePath =
  //   "https://medhr.medicine.psu.ac.th/app-api/v3/?/apis/staff-mobile/person_id/" +
  //   perid +
  //   "/contact_type_id/1/?fields=data&limit=1";
  // https
  //   .get(mobilePath, (res) => {
  //     // console.log("statusCode:", res.statusCode);
  //     // console.log("headers:", res.headers);
  //     console.log(res);
  //     res.on("data", (d) => {
  //       console.log(d);
  //       process.stdout.write(d);
  //     });
  //   })
  //   .on("error", (e) => {
  //     console.error(e);
  //   });
  //   sendSMS();
  res.send("Test Send SMS");
});

app.post("/api/perid", (req, res) => {
  let body = req.body;
  let perid = body.perid;
  const pool = mysql.createPool({
    connectionLimit: 10,
    host: "172.29.1.11",
    user: "rpmmis",
    password: "*rpmmis*",
    database: "STAFF",
  });

  // console.log(perid);
  sql = "SELECT PERID, NAME, SURNAME, PIN FROM Medperson WHERE PERID = ?";
  let values = [perid];

  pool.query(sql, values, (err, response) => {
    let result = [];
    if (err) {
      result = {
        statusCode: 400,
        data: err,
      };
    } else {
      result = {
        statusCode: 200,
        data: response,
      };
    }
    res.send(result);
  });
  pool.close();
});

app.post("/api/timezone", (req, res) => {
  var d = new Date().toString();
  console.log(d);
  res.send("Test Timezone");
});

app.post("/api/smartmedpsu/pushnotilog", (req, res) => {
  let body = req.body;
  let token = body.token;
  let notitoken = body.notitoken;

  let decode = jwt.decode(token, "smartmedpsu");
  console.log(decode.id);
  console.log("Noti Token: " + notitoken);

  // logs.createLogFile("resultlogs", "success", val);
  const pool = mysql.createPool({
    connectionLimit: 10,
    host: "172.29.70.129",
    user: "famz",
    password: "quiu7eedem",
    database: "smartmedpsu_notitoken",
  });

  // console.log(perid);
  sql = "SELECT * FROM user_token WHERE PERID = ?";
  pool.query(sql, [decode.id], (err, response) => {
    let result = [];
    if (err) {
      result = {
        statusCode: 400,
        data: err,
      };
    } else {
      let notilogresult = [];
      let values = [];
      if (response.length != 0) {
        console.log("อัพเดท Noti Token");
        sql =
          "UPDATE user_token SET NOTI_TOKEN = ?, UPDATED_DATE = ?, UPDATED_TIME = ? WHERE PERID = ?";
        values = [notitoken, new Date(), new Date(), decode.id];
      } else {
        console.log("เพิ่ม Noti Token");
        sql =
          "INSERT INTO user_token(PERID, TOKEN, NOTI_TOKEN, UPDATED_DATE, UPDATED_TIME) VALUES(?)";
        values.push([decode.id, token, notitoken, new Date(), new Date()]);
      }

      pool.query(sql, values, (err, notilogresponse) => {
        if (err) {
          notilogresult = {
            statusCode: 400,
            data: err,
          };
        } else {
          notilogresult = {
            statusCode: 200,
            data: "success",
          };
        }
        res.send(notilogresult);
      });
    }
  });

  pool.close();
});

const port = process.env.PORT || 5555;
app.listen(port, "0.0.0.0", () =>
  console.log("Listening on port " + port + "...")
);
